package com.example.voicebasedprescription;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Welcome_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        getSupportActionBar().setTitle("Welcome Page");
    }

    public void btn_LoginForm(View view) {
        startActivity(new Intent(getApplicationContext(),Login_Form.class));
    }
}